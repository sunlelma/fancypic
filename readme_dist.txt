=========================================
==== sunl's FancyBoost Picture Generator ====
=========================================

Welcome to sunl's FancyBoost Picture Generator.

How it works is you run fancypic.exe, select an image file and a destination, press "CREATE FANCYBOOST PICTURE LEVEL!" and a custom image is transported into your elma level, which you can then edit with other level editors.

Your lgrs need to be patched with the LGR_Booster_Patch.exe - Simply click "Choose folder", select your elma/lgr folder, then press "FancyBOOST" to patch your lgr files.

The only other important thing is that the size of your image must be a maximum of 5000 because that's the amount supported by elma. This excludes the alpha channel which is by default "#FF00FF" in color. In addition, if you have the same color beside itself on a horizontal line, it will only count as 1. If you try with an image that is too big, the program will tell you by how much you need to shrink the image.

You can also scale the pixels to be larger or smaller.

That's all you need to know to use the program. However, you may continue reading for more details & tips.

The original retro version of fancypic is included in the retro folder.

==== Alpha Channel ====
To set a transperant pixel, you can choose 1 pixel color that will be transperant. Write the rgb value of this pixel under "Alpha channel" in the format "#33AAFF". The default alpha channel is "#FF00FF", but any color can be set, or there can be no alpha channel ("None").

If you use an alpha channel, you will almost definitively need to set the picture to be clipped to only sky and ground, or else the image will appear to be a mess.

You can also set a solid-nocolor channel. This pixel will count as solid in the sense that a polygon will make it solid, but no picture will be drawn at this point. This can be used to expand the size of an image by losing pixel-perfect detail.


==== My Image Requires Too Many Pixels! ====
The best way to reduce pixel use is first of all to reduce image size. The next thing is to make sure as many horizontal lines contain the same color. To do this you can use a program such as GIMP and by clicking on Image->Mode->Indexed..., you can decrease the number of colors. You can still use as many colors as you want, but try to make patches of similar colors instead of a soup of different colors at every pixel.

==== Extra Details about the Program ====
When you choosing the image file and save file, you can write the full paths (C:/Elma/FancyPic/picture.png), or relative paths (picture.png)

The generated image has one distance defined by you.

You can also select the clipping of the generated image.

For the alpha channel, you can omit the "#" (so "33AAFF" would be equivalent to "#33AAFF")

The picture-polygon offset moves the polygons by a fixed amount in order to make sure the pictures and polygons are aligned. Adjusting this value is not recommended. A value of 0 is centered. A value of 1 means the polygon is 1 image pixel away.

The pixel's polygonal radius is the fatness of a single line of solid pixels or a solid dot. You do not need to edit this number for normal use. Changing this value is untested. This value should only affect the pixels on the border of transperant pixels.

Image size scale allows you to make an image larger or smaller. However, a round number causes line glitches and so a value of 0.99984 is used instead. As smaller or larger values produce very wonky effects, you do not need to edit this number for normal use.


==== Included Files ====

GimpPalettes/ElmaPalette.gpl - GIMP palette for better color control (see Better Image Quality below)
fancy_lgr_data.dat - A datafile containing info about the pictures in default.lgr in order to generate the images
fancypic.exe - the executable file running the program
readme.txt - hello
Sample1.png, etc. - example pictures for the program.

==== Better Image Quality ====
An image-editing software can allow you to have more control over the colors that are actually drawn in the level.

The two gpl files in the folder GimpPalettes are palette files that can be imported into GIMP to have full control over the desired color. The instructions below take about 5 minutes to learn.

0) Install GIMP if you don't have it. (https://www.gimp.org/downloads/)
1) Open up GIMP, then at the top click Edit->Preferences.
2) Navigate to Folders/Palettes
3) There should be a checkbox next to a filepath, such as "C:\Users\**user**\.gimp-2.8\palettes"
4) Navigate to this folder
5) Paste ElmaPalette.gpl and into "C:\Users\**user**\.gimp-2.8\palettes"
6) Open up your desired image on GIMP
7) At the top, click on Image->Mode->Indexed...
8) Select "Use custom palette", then choose "ElmaPalette"
9) Uncheck "Remove unused colors from colormap"
10) Press Convert
11) At the top, click on Windows->Dockable Dialogs->Palettes
12) In the new window, double-click on "ElmaPaletteAlpha" at the top of the list
13) There should now be a window called "Palette Editor" that you can use to select colors to edit/recolor your image
14) Edit/recolor your image (and save the file if you want)
15) Feel free to go back into Image->Mode->RGB to add the alpha channel color
15) When finished, you can export your image by going to File->Export As..., and then save it as any common lossless image file (e.g. bmp, png)
16) You can now have an exact representation of your image in elma by using FancyPic.exe


==== About ====
Made by sunl over the course of a day on January 5, 2017

This program uses sigvef's Elma Python Library (https://github.com/sigvef/elma)
Smibu's Elmanager's code was consulted to understand and solve issues with the orientation of polygons (https://gitlab.com/Smibu/elmanager)
The executable stand-alone file was generated with pyinstaller


==== Changelog ====
2.01 (2017/02/13):
                   -Integrated retro and boost versions together
                   -Renamed to FancyBoost Picture Generator
2.00 (2017/02/10):
                   -Upgraded fancypic to only work with boosted lgrs.
                   -Fixed a bug with image scaling causing bad pictures at zooms of not 1
                   -Changed picture-polygon offset to be relative to 0 (0.5 -> 0). New recommended default is 0
                   -Added support for 256 colors using lgr boosting.
                   -fancy_lgr_data.dat updated with new data
                   -Added detection to use fewer pictures when the same color is used several times in a row.
                       -Now images can have sometimes 10x the detail
1.06 (2017/02/04):
                   -Originally image scaling was called "paint-pixel offset" and was set to 0.2083 (=1/48*0.99984). This number was changed to be relative to the number 1 instead of absolute
                   -Added a solid-nocolor channel, defaulting to #AA00AA
                   -Updated ElmaPaletteAlpha to contain the nodraw channel
                   -Fixed a bug checking if the polygonal radius was valid
1.05 (2017/01/14):
                   -Changed alpha channel to default to #FF00FF
                   -Added GIMP palette version with alpha channel (ElmaPaletteAlpha.gpl), and added picture names to each color
                   -Changed default clipping to "G"
                   -Added polygon generation (with option picture-polygon offset & pixel's polygonal radius)
                   -Behind-the-scenes code cleanup, and creation of batch .exe file maker
1.03 (2017/01/05): Added alpha channel
1.02 (2017/01/05): Stand-alone GUI and GIMP palette (ElmaPaletteAlpha.gpl) created. Converts an image into a picture only
1.01 (2017/01/05): Initial stable python code written