@echo This will delete savefile.lev, fancypic.spec, dist/ and build/
@pause

del savefile.lev
del fancypic.spec
rmdir dist /s /q
rmdir build /s /q
rmdir output /s /q

pause