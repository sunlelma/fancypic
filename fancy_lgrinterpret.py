from PIL import Image
from PIL import ImageDraw
from fancy_general import PaintPixel
import os
import pickle

LGR_DEFAULT_PALETTE=[0, 0, 0, 120, 48, 0, 32, 0, 0, 180, 196, 172, 156, 0, 0, 164,
	0, 0, 96, 96, 56, 0, 8, 8, 8, 0, 8, 56, 0, 0, 104, 96, 104, 96, 104, 104, 156,
	24, 8, 80, 8, 72, 244, 112, 0, 80, 0, 0, 156, 40, 8, 180, 0, 0, 156, 40, 0, 120,
	0, 0, 136, 64, 16, 156, 16, 8, 40, 8, 0, 0, 128, 8, 0, 128, 0, 244, 228, 16, 128,
	0, 0, 56, 48, 48, 32, 32, 48, 0, 96, 0, 188, 204, 188, 0, 0, 48, 136, 40, 0, 64,
	24, 8, 180, 196, 180, 56, 16, 0, 40, 112, 180, 196, 96, 16, 0, 8, 0, 136, 180,
	212, 0, 104, 0, 104, 40, 0, 156, 104, 8, 136, 24, 0, 156, 156, 72, 220, 228, 220,
	0, 96, 8, 96, 0, 0, 128, 128, 128, 136, 136, 96, 120, 128, 148, 72, 64, 32, 0,
	212, 24, 96, 96, 96, 104, 96, 96, 148, 48, 0, 136, 48, 196, 120, 32, 0, 156, 156,
	96,	136, 156, 80, 136, 164, 204, 0, 48, 120, 0, 80, 0, 48, 156, 48, 104, 112, 96,
	72,	72, 72, 104, 112, 112, 188, 196, 180, 252, 112, 0, 0, 128, 24, 96, 148, 196,
	96,	148, 204, 96, 148, 148, 16, 96, 172, 164, 64, 8, 148, 180, 212, 196, 0, 0, 0,
	48, 0, 148, 0, 0, 172, 0, 0, 136, 48, 0, 0, 32, 0, 64, 16, 0, 180, 188, 188, 0,
	136, 8, 72, 96, 72, 0, 172, 0, 148, 16, 196, 244, 212, 16, 120, 48, 16, 148, 40,
	0, 196, 0, 8, 128, 128, 148, 148, 16, 0, 24, 8, 0, 80, 24, 0, 8, 8, 80, 16, 56,
	24, 180, 180, 172, 24, 96, 172, 156, 32, 136, 56, 8, 0, 40, 56, 64, 128, 48, 8,
	120, 164, 204, 120, 164, 112, 16, 0, 0, 188, 180, 64, 236, 188, 56, 32, 32, 32,
	164, 180, 172, 0, 120, 8, 80, 80, 80, 0, 120, 0, 136, 0, 0, 24, 96, 180, 0, 120,
	16, 104, 156, 204, 16, 112, 0, 104, 156, 180, 204, 220, 196, 16, 112, 16, 88, 8,
	0, 104, 0, 8, 8, 112, 0, 0, 112, 8, 88, 0, 0, 72, 40, 0, 0, 112, 0, 104, 104, 96,
	8, 96, 0, 104, 120, 120, 196, 8, 8, 56, 64, 64, 0, 64, 0, 148, 104, 96, 56, 64,
	32, 96, 0, 164, 228, 96, 8, 16, 16, 8, 16, 16, 16, 32, 104, 180, 112, 156, 204,
	180, 156, 48, 16, 24, 16, 148, 16, 204, 32, 0, 8, 148, 56, 8, 24, 16, 72, 80, 136,
	196, 80, 88, 88, 56, 120, 188, 120, 16, 0, 64, 128, 188, 72, 128, 188, 148, 188,
	212, 48, 136, 40, 48, 120, 188, 120, 32, 8, 120, 8, 0, 80, 80, 56, 72, 0, 0, 64,
	64, 64, 96, 96, 88, 136, 24, 8, 16, 32, 8, 40, 0, 0, 72, 80, 64, 40, 48, 40, 16,
	120, 8, 48, 104, 188, 0, 16, 8, 88, 136, 196, 156, 8, 0, 96, 128, 80, 120, 120,
	120, 72, 136, 196, 56, 24, 0, 180, 8, 8, 120, 120, 72, 120, 96, 32, 8, 180, 8,
	72, 128, 196, 136, 72, 32, 0, 112, 32, 24, 104, 180, 88, 24, 16, 96, 64, 24, 188,
	204, 180, 136, 136, 148, 48, 112, 180, 24, 24, 24, 16, 56, 0, 120, 172, 204, 8,
	16, 24, 80, 212, 40, 104, 212, 244, 0, 96, 204, 104, 48, 16, 96, 96, 204, 104, 0,
	0, 80, 136, 188, 164, 156, 164, 148, 0, 80, 188, 32, 48, 8, 88, 172, 32, 112, 24,
	56, 72, 56, 0, 80, 172, 252, 164, 32, 220, 164, 24, 16, 96, 112, 8, 88, 180, 32,
	24, 24, 136, 252, 0, 40, 112, 188, 120, 136, 128, 236, 220, 72, 32, 40, 64, 120,
	48, 8, 104, 164, 196, 244, 164, 120, 236, 156, 120, 120, 64, 16, 188, 16, 8, 96,
	24, 0, 40, 16, 8, 64, 120, 188, 0, 16, 0, 64, 212, 24, 72, 228, 8, 56, 40, 212,
	32, 228, 40, 104, 148, 196, 0, 88, 172, 16, 128, 16, 196, 204, 196, 8, 80, 16, 220,
	244, 220, 236, 16, 48, 40, 16, 0, 40, 104, 180, 120, 156, 220, 88, 16, 212, 48, 48,
	80, 88, 148, 196, 220, 0, 0, 212, 212, 212, 0, 8, 156, 0, 148, 196, 88, 80, 80, 72,
	220, 40, 16, 80, 172, 228, 128, 96, 204, 64, 24, 252, 252, 252]

def chunk(seq, size, groupByList=True):
	"""Returns list of lists/tuples broken up by size input"""
	func = tuple
	if groupByList:
		func = list
	return [func(seq[i:i + size]) for i in range(0, len(seq), size)]

def PaintPixelMyPaletteSort(var):
	return var.mypalette

	
"""
	Takes the images of an lgr file and generates a fancy_lgr_data.dat datafile to be used in FancyPic. The datafile is specific to the lgr file used.
	
	Inputs:
		All the images from an lgr converted from .pcx to .bmp, retaining palette information. The images may be obtained using EasyLGR.exe and then converted using an online converter or GIMP.
		The images must be saved in a folder. However, non-picture images must be manually removed. These include all image files starting with "Q" (e.g. "QKILLER"), masks (e.g. "maskbig") and textures (e.g. "brick")
		This program assumes that the topleft pixel is the alpha color, which may not necessarily be true for custom lgrs.
        TRANSPARENT_TOPLEFT: True if topleft is transparent, False if no transparency
	Output:
		fancy_lgr_data.dat: A list of PaintPixel objects, where each PaintPixel object contains data about one image
"""
def makeDataFile(TRANSPARENT_TOPLEFT=True,folder="LGRpic",savefile="Retro/fancy_retro.dat"):
    filelist=[]	#Make a list of all bmps in LGRpic
    for file in os.listdir(folder):
        if file.endswith(".bmp"):
            filelist.append(file)

    #All images in an lgr have same palette. Start by getting the palette from the first image
    img=Image.open(folder+"/"+filelist[0])
    palette=chunk(img.getpalette(),3,True) #getpalette returns a list of 256*3=768 bytes representing the palette. chunk converts this to a list of 256 triplets

    #find the colored pixel that is leftmost in the topmost row with colored pixels for each picture
    PaintPixelList=[]
    for file in filelist:
        img=Image.open(folder+"/"+file)
        if(TRANSPARENT_TOPLEFT):
            alpha=img.getpixel((0,0)) #The topleft corner is assumed to be the transperant color. This is true for default.lgr but not always true for custom lgrs.
        else:
            alpha=None
        found=False
        for i in range(0,img.height):
            for j in range(0,img.width):
                if(img.getpixel((j,i))==alpha):	#skip transperant pixels until coming to the first colored pixel.
                    continue
                found=True
                PaintPixelList.append(PaintPixel(img.getpixel((j,i)),(j,i),palette[img.getpixel((j,i))],file[:-4],img.width,img.height))	#save all relevant info into a PaintPixel object in the PaintPixelList masterlist
                break
            if(found):
                break
            continue
        continue

    #purge duplicates with the same palette number
    PaintPixelList=sorted(PaintPixelList,key=PaintPixelMyPaletteSort) #sort in ascending order by mypalette
    i=0
    while i<(len(PaintPixelList)-1):
        if(PaintPixelList[i].mypalette==PaintPixelList[i+1].mypalette): #If two images have the same palette index they represent the same color
            if(PaintPixelList[i].width+PaintPixelList[i].height<PaintPixelList[i+1].width+PaintPixelList[i+1].height): #delete the one with the smallest dimensions
                del PaintPixelList[i+1]
            else:
                del PaintPixelList[i]
            continue	#don't increment yet as there may be 3 with the same palette index
        i=i+1

    with open("fancy_lgr_data.dat","wb") as output:
        pickle.dump(PaintPixelList,output,pickle.HIGHEST_PROTOCOL)	#save into fancy_lgr_data.dat

    print("Success")

def generate_Boost():
    img=Image.new("P",(200,201),0)
    img.putpalette(LGR_DEFAULT_PALETTE)
    draw=ImageDraw.Draw(img)
    draw.line([0,200,200,200],fill=1,width=1)
    del draw
    img.save("output/zz%0.2X%0.2X%0.2X.bmp"%(LGR_DEFAULT_PALETTE[0],LGR_DEFAULT_PALETTE[1],LGR_DEFAULT_PALETTE[2]),'bmp')
    for i in range(1,256):
        img=Image.new("P",(200,200),i)
        img.putpalette(LGR_DEFAULT_PALETTE)
        img.save("output/zz%0.2X%0.2X%0.2X.bmp"%(LGR_DEFAULT_PALETTE[i*3],LGR_DEFAULT_PALETTE[i*3+1],LGR_DEFAULT_PALETTE[i*3+2]),'bmp')
       
makeDataFile(TRANSPARENT_TOPLEFT=True,folder="LGRpic",savefile="Retro/fancy_retro.dat")
generate_Boost()
makeDataFile(False,"output","fancy_boost.dat")