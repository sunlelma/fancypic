import tkinter as tk
import tkinter.filedialog as filedialog
from fancy_core import picturemake
from fancy_general import Hex2Rgb

class Application(tk.Frame):
    def __init__(self,master):
        super().__init__(master)
        self.pack()
        self.create_widgets()
        
    def create_widgets(self):
    
        self.button1=tk.Button(self,text="Choose image file",command=self.SelectImagePopup)
        self.button1.pack(side="top")
        self.label1=tk.Entry(self,width=80)
        self.label1.insert(0,"Image File")
        self.label1.pack(side="top")
        
        self.button2=tk.Button(self,text="Choose save file",command=self.SelectSavePopup)
        self.button2.pack(side="top")
        self.label2=tk.Entry(self,width=80)
        self.label2.insert(0,"savefile.lev")
        self.label2.pack(side="top")
        
        self.label3=tk.Label(self,text="Picture distance (1-999)")
        self.label3.pack(side="top")
        self.entry3=tk.Entry(self,validate="focusout",validatecommand=self.CheckTextDistance)
        self.entry3.insert(0,"600")
        self.entry3.pack(side="top")

        self.label4=tk.Label(self,text="Clipping (U,G,S)")
        self.label4.pack(side="top")
        self.entry4=tk.Entry(self,validate="focusout",validatecommand=self.CheckTextClipping)
        self.entry4.insert(0,"G")
        self.entry4.pack(side="top")
        
        self.label6=tk.Label(self,text="Alpha channel (Write \"None\" or rgb hex (\"#000000\"-\"#FFFFFF\")")
        self.label6.pack(side="top")
        self.entry6=tk.Entry(self,validate="focusout",validatecommand=self.CheckTextAlpha)
        self.entry6.insert(0,"#FF00FF")
        self.entry6.pack(side="top")
        
        self.label9=tk.Label(self,text="Solid-nocolor channel (Write \"None\" or rgb hex (\"#000000\"-\"#FFFFFF\")")
        self.label9.pack(side="top")
        self.entry9=tk.Entry(self,validate="focusout",validatecommand=self.CheckTextNoColor)
        self.entry9.insert(0,"#AA00AA")
        self.entry9.pack(side="top")
        
        self.label5=tk.Label(self,text="Image size scale (to avoid horizontal line bugs, do not use full numbers (e.g. use 2.999 instead of 3))")
        self.label5.pack(side="top")
        self.entry5=tk.Entry(self,validate="focusout",validatecommand=self.CheckPixelSize)
        self.entry5.insert(0,"0.99984")
        self.entry5.pack(side="top")
        
        self.label7=tk.Label(self,text="Advanced: Picture-Polygon offset (in number of pixels)")
        self.label7.pack(side="top")
        self.entry7=tk.Entry(self,validate="focusout",validatecommand=self.CheckPicPolyOffset)
        self.entry7.insert(0,"0")
        self.entry7.pack(side="top")
        
        self.label8=tk.Label(self,text="Advanced: 1 pixel's polygonal radius (recommend 0.45. Max 1.49)")
        self.label8.pack(side="top")
        self.entry8=tk.Entry(self,validate="focusout",validatecommand=self.CheckNESWDist)
        self.entry8.insert(0,"0.45")
        self.entry8.pack(side="top")
        
        self.frame10=tk.Frame(self)
        self.frame10.pack(side="top")
        self.button100=tk.Button(self.frame10,text="(Retro FancyPic creation)",command=self.CreateLevelRetro,fg="gray")
        self.button100.pack(side="left")
        self.button101=tk.Button(self.frame10,text="CREATE FANCYBOOST PICTURE LEVEL!",command=self.CreateLevel,fg="red")
        self.button101.pack(side="left")
        
        
    def SelectImagePopup(self):
        imgfile=filedialog.askopenfilename(filetypes=[("Image Files","*.jpg;*.gif;*.png;*.bmp"),("All","*")])
        self.label1.delete(0,len(self.label1.get()))
        self.label1.insert(0,imgfile)
        
    def SelectSavePopup(self):
        savename=filedialog.asksaveasfilename(filetypes=[("Elastomania Level File","*.lev"),("All","*")])
        if(savename[-4:]!=".lev"):
            savename=savename+".lev"
        self.label2.delete(0,len(self.label2.get()))
        self.label2.insert(0,savename)
        
    def CheckTextDistance(self):
        #Valid entries for textbox "Distance": 1-999
        try:
            dist=int(self.entry3.get())
            if(dist>=1 and dist<=999):
                return True
            self.entry3.delete(0,len(self.entry3.get()))
            self.entry3.insert(0,"600")
            return True
        except ValueError:
            self.entry3.delete(0,len(self.entry3.get()))
            self.entry3.insert(0,"600")
            return True
            
    def CheckTextClipping(self):
        #Valid entries for textbox "Clipping": "U", "G", "S"
        clip=self.entry4.get()
        if(clip=="U" or clip=="G" or clip=="S"):
            return True
        self.entry4.delete(0,len(self.entry4.get()))
        self.entry4.insert(0,"G")
        return True

    def CheckPixelSize(self):
        #Valid entries for textbox "length/pixel": 0.1-100. We use 0.99984 instead of 1 because 1 causes pixel-line bugs
        try:
            pw=float(self.entry5.get())
            if(pw>=0.1 and pw<=100):
                return True
            self.entry5.delete(0,len(self.entry5.get()))
            self.entry5.insert(0,"0.99984")
            return True
        except ValueError:
            self.entry5.delete(0,len(self.entry5.get()))
            self.entry5.insert(0,"0.99984")
            return True

    def CheckTextAlpha(self):
        #Valid entries for textbox "Alpha": "None", "#000000"-"#FFFFFF", "000000"-"FFFFFF"
        try:
            hex=self.entry6.get()
            if(hex=="None"):
                return true
            Hex2Rgb(hex)
            return True
        except:
            self.entry6.delete(0,len(self.entry6.get()))
            self.entry6.insert(0,"#FF00FF")
            return True

    def CheckPicPolyOffset(self):
        #Valid entries for textbox "Picture-Polygon distance": -10 to 10
        try:
            pw=float(self.entry7.get())
            if(pw>=-10 and pw<=10):
                return True
            self.entry7.delete(0,len(self.entry7.get()))
            self.entry7.insert(0,"0")
            return True
        except ValueError:
            self.entry7.delete(0,len(self.entry7.get()))
            self.entry7.insert(0,"0")
            return True
            
    def CheckNESWDist(self):
        #Valid entries for textbox: 0<1.5
        try:
            pw=float(self.entry8.get())
            if(pw>0 and pw<1.5):
                return True
            self.entry8.delete(0,len(self.entry8.get()))
            self.entry8.insert(0,"0.45")
            return True
        except ValueError:
            self.entry8.delete(0,len(self.entry8.get()))
            self.entry8.insert(0,"0.45")
            return True
            
    def CheckTextNoColor(self):
        #Valid entries for textbox "Solid-nocolor": "None", "#000000"-"#FFFFFF", "000000"-"FFFFFF"
        try:
            hex=self.entry9.get()
            if(hex=="None"):
                return true
            Hex2Rgb(hex)
            return True
        except:
            self.entry9.delete(0,len(self.entry9.get()))
            self.entry9.insert(0,"#AA00AA")
            return True
            
    def CreateLevel(self):
        a=self.label1.get()
        b=self.label2.get()
        c=self.entry3.get()
        d=self.entry4.get()
        e=self.entry5.get()
        f=self.entry6.get()
        g=self.entry7.get()
        h=self.entry8.get()
        i=self.entry9.get()
        picturemake(a,b,c,d,e,f,g,h,i,True)
        return
        
    def CreateLevelRetro(self):
        a=self.label1.get()
        b=self.label2.get()
        c=self.entry3.get()
        d=self.entry4.get()
        e=self.entry5.get()
        f=self.entry6.get()
        g=self.entry7.get()
        h=self.entry8.get()
        i=self.entry9.get()
        picturemake(a,b,c,d,e,f,g,h,i,False)
        return
        
root=tk.Tk()
root.title("sunl's FancyBoost Picture Generator")
app=Application(master=root)

app.mainloop()
