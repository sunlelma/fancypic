=========================================
==== sunl's Fancy Picture Generator ====
=========================================

Welcome to sunl's Fancy Picture Generator.

How it works is you run fancypic.exe, select an image file and a destination, press "Create!" and a custom image is transported into your elma level, which you can then edit with other level editors.

The only important thing is that the number of pixels in the image must be a maximum of 5000 because that's the amount supported by elma. This excludes the alpha channel which is by default "#FF00FF" in color. For example, 5000 pixel is equivalent to solid images of the following size: (70x70), (50x100), (25x200). If you try with an image that is too big, the program will tell you by how much you need to shrink the image.

The images are best at zooms of 1.00, 2.00, 3.00. The image is worse at 1.25, 1.50 or 1.75 due to zooming glitches.

That's all you need to know to use the program. However, you may continue reading for more details & tips.


==== Color Limitations ====

There are only 19 available colors that can be made. The program will automatically convert your image so that the closest colors will match. However, there are many missing colors (e.g. purple and yellow).

The available colors are shown in "AvailableColors.png". Scroll down to "Better Image Quality" to see what you can do to produce better images for your elma levels.


==== Alpha Channel ====
To set a transperant pixel, you can choose 1 pixel color that will be transperant. Write the rgb value of this pixel under "Alpha channel" in the format "#33AAFF". The default alpha channel is "#FF00FF", but any color can be set, or there can be no alpha channel ("None").

If you use an alpha channel, you will almost definitively need to set the picture to be clipped to only sky and ground, or else the image will appear to be a mess.

You can also set a solid-nocolor channel. This pixel will count as solid in the sense that a polygon will make it solid, but no picture will be drawn at this point. This can be used to expand the size of an image by losing pixel-perfect detail.


==== Extra Details about the Program ====
When you choosing the image file and save file, you can write the full paths (C:/Elma/FancyPic/picture.png), or relative paths (picture.png)

The generated image has one distance defined by you.

You can also select the clipping of the generated image.

For the alpha channel, you can omit the "#" (so "33AAFF" would be equivalent to "#33AAFF")

The picture-polygon offset moves the polygons by a fixed amount in order to make sure the pictures and polygons are aligned. Adjusting this value will make the image look ever so slightly better at the sky/ground border. The best offset is 1 if the zoom of the players is 1.00, and at increased zoom, 0.75 looks slightly better but a bit of detail is lost for the players at 1.00 zoom. If you move the pictures and polygons around in an editor, this number may not apply as well as initially. Therefore, in theory you should instead move the whole level around the image if you care that much.

The pixel's polygonal radius is the fatness of a single line of solid pixels or a solid dot. You do not need to edit this number for normal use. Changing this value is untested. This value should only affect the pixels on the border of transperant pixels.

Image size scale allows you to make an image larger or smaller. However, scaling an image usually results in color distortions. A value of 1 means 1 picture is put every 1 pixel in elastomania. However, a value of exactly 1 causes line glitches and so a value of 0.99984 is used instead.


==== Included Files ====

GimpPalettes/ElmaPalette.gpl and ElmaPaletteAlpha.gpl - GIMP palettes for better color control (see Better Image Quality below)
AvailableColors.png - this file shows the 19 colors that the program can draw into elastomania
fancy_retro.dat - A datafile containing info about the pictures in default.lgr in order to generate the images
fancypic.exe - the executable file running the program
readme.txt - hello
Sample1.png, etc. - example optimized pictures for the program. You can also use unoptimized pictures but they won't look as good.


==== Better Image Quality ====
The only available colors that can be generated are shown in AvailableColors.png.

Briefly, the colors are black, grey, blue, and multiple shades of brown, green and red. The other colors are unavailable (e.g. pure white, purple, yellow).

An image-editing software can allow you to have more control over the colors that are actually drawn in the level.

The two gpl files in the folder GimpPalettes are palette files that can be imported into GIMP to have full control over the desired color. The instructions below take about 5 minutes to learn.

0) Install GIMP if you don't have it. (https://www.gimp.org/downloads/)
1) Open up GIMP, then at the top click Edit->Preferences.
2) Navigate to Folders/Palettes
3) There should be a checkbox next to a filepath, such as C:\Users\**user**\.gimp-2.8\palettes
4) Navigate to this folder
5) Paste ElmaPalette.gpl and ElmaPaletteAlpha.gpl into C:\Users\**user**\.gimp-2.8\palettes
6) Open up your desired image on GIMP
7) At the top, click on Image->Mode->Indexed...
8) Select "Use custom palette", then choose "ElmaPalette"
9) Uncheck "Remove unused colors from colormap"
10) Press Convert
11) At the top, click on Image->Mode->RGB
12) At the top, click on Image->Mode->Indexed...
13) Select "Use custom palette", then choose "ElmaPaletteAlpha"
14) Uncheck "Remove unused colors from colormap"
15) Press Convert
16) At the top, click on Windows->Dockable Dialogs->Palettes
17) In the new window, double-click on "ElmaPaletteAlpha" at the top of the list
13) There should now be a window called "Palette Editor" that you can use to select colors to edit/recolor your image
14) Edit/recolor your image (and save the file if you want)
15) When finished, you can export your image by going to File->Export As..., and then save it as any common lossless image file (e.g. bmp, png)
16) You can now have an exact representation of your image in elma by using FancyPic.exe


==== About ====
Made by sunl over the course of a day on January 5, 2017

This program uses sigvef's Elma Python Library (https://github.com/sigvef/elma)
Smibu's Elmanager's code was consulted to understand and solve issues with the orientation of polygons (https://gitlab.com/Smibu/elmanager)
The executable stand-alone file was generated with pyinstaller


==== Changelog ====
1.07 (2017/02/10):
                   -Fixed a bug with image scaling causing bad pictures at zooms of not 1
1.06 (2017/02/04):
                   -Originally image scaling was called "paint-pixel offset" and was set to 0.2083 (=1/48*0.99984). This number was changed to be relative to the number 1 instead of absolute
                   -Added a solid-nocolor channel, defaulting to #AA00AA
                   -Updated ElmaPaletteAlpha to contain the nodraw channel
                   -Fixed a bug checking if the polygonal radius was valid
1.05 (2017/01/14):
                   -Changed alpha channel to default to #FF00FF
                   -Added GIMP palette version with alpha channel (ElmaPaletteAlpha.gpl), and added picture names to each color
                   -Changed default clipping to "G"
                   -Added polygon generation (with option picture-polygon offset & pixel's polygonal radius)
                   -Behind-the-scenes code cleanup, and creation of batch .exe file maker
1.03 (2017/01/05): Added alpha channel
1.02 (2017/01/05): Stand-alone GUI and GIMP palette (ElmaPaletteAlpha.gpl) created. Converts an image into a picture only
1.01 (2017/01/05): Initial stable python code written