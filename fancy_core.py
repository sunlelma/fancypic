from elma.models import Level
from elma.models import Obj
from elma.models import Picture
from elma.models import Point
from elma.models import Polygon
from elma.packing import pack_level
from PIL import Image
from fancy_general import PaintPixel
from fancy_general import Hex2Rgb
from fancy_general import Truncate
import pickle
import ctypes
import sys
import math
import logging
logging.basicConfig(level=logging.ERROR, format='%(message)s')		#CHANGE LOGGING LEVEL TO logging.ERROR from logging.DEBUG

ZZ_IMAGE_WIDTH=200

def FindTouchingPixelAround(edgematrix,coord,start_index,trans,direction):
    """
        Searches clockwise or counterclockwise in the 8 pixels around a pixel to find the next non-transperant pixel
        
        Inputs:
            edgematrix. Variable passed from picturemake. See below 
            coord ((i,j))
                i (int): The x coordinate of the central pixel in edgematrix
                j (int): The y coordinate of the central pixel in edgematrix
            start_index (int): an index number 0-7 corresponding to a pixel around the central pixel defined by the dictionary index2coord. See documentation "index2coord". The search will start from this pixel
            trans [location,parity,angle]: Used to transform the coordinates to analyze the situation from a different angle/direction. See CoordRotate and documentation "transformation"
            direction (int): -1=search counterclockwise. 1=search clockwise

            
        Output:
            A number from 0-7 corresponding to the index (index2coord) of the first found non-transperant pixel. returns -1 if all 8 pixels are transperant
    """
    logging.info("  --FindTouchingPixelAround--")
    for count in range(8):
        offset=CoordRotate(index2coord[(start_index+count*direction)%8],trans)
        logging.debug("  x:"+str(offset[0])+",y:"+str(offset[1]))
        if(edgematrix[coord[0]+offset[0]][coord[1]+offset[1]][0]):
            logging.info("  found:"+str((start_index+count*direction)%8))
            logging.info("  --end--")
            return (start_index+count*direction)%8
        logging.info("  not found: -1")
        logging.info("  --end--")
    return -1
    #transform 
    
def CoordRotate(coord,trans):
    """
        Instead of transforming the whole edgematrix, does the opposite transformation to a coordinate to analyze a situation form a specific angle/direction.
        
        Inputs:
            coord ((i,j)): the relative coordinate to transform according to trans
                i (int): x
                j (int): y
            trans [location,parity,angle]: Used to transform the coordinates to analyze the situation from a different angle/direction. See and documentation "transformation"
                location (int): 0=the pixel around is adjacent to the central pixel. 1=the pixel around is diagonal to the central pixel. Unused in this function
                parity (int) 0=do nothing. 1=the edgematrix would have to be flipped along a vertical axis AFTER the rotation is applied. This function does the opposite effect to the coordinate
                angle (int) the edgematrix would have to be rotated by angle*(90 degrees). This function does the opposite effect to the coordinate
        Output:
            The transformed coord
    """
    #Does not spin the matrix, to fit the situation, but rather spins the coordinate targeting system the opposite direction
    #transformation=[location,parity,angle]
    #note that in theory, to "spin the matrix", we would first rotate, and then flip along a vertical line. Reverse: flip vertical, then reverserotate
    coord=[coord[0]*(1-2*trans[1]),coord[1]]	#if parity=1, flip along vertical axis
    return [(coord[0]*cosd[trans[2]]+coord[1]*sind[trans[2]]),(coord[0]*sind[trans[2]]*(-1)+coord[1]*cosd[trans[2]])]	#multiply coordinates by a rotation matrix to rotate the coordinates in the inverse direction
    
def BinaryRotate(nesw,trans):
    """
        Transforms nesw info so that it can be correctly applied to an edgematrix pixel. We do the reverse transformation as we need to change an unaltered nesw to be rotated on top of an unaltered matrix
        
        Inputs:
            nesw (int): binary 0b0000 to 0b1111, where 1=the programm has processed the corresponding edge of the pixel in question. THe nesw passed to this function will be ORed with the edgematrix
                0b1000=north edge, 0b0100 east, 0b0010 south, 0b0001 west
            trans [location,parity,angle]: Used to transform the coordinates to analyze the situation from a different angle/direction. See and documentation "transformation"
                location (int): 0=the pixel around is adjacent to the central pixel. 1=the pixel around is diagonal to the central pixel. Unused in this function
                parity (int) 0=do nothing. 1=the edgematrix would have to be flipped along a vertical axis AFTER the rotation is applied. This function does the opposite effect to the coordinate
                angle (int) the edgematrix would have to be rotated by angle*(90 degrees). This function does the opposite effect to the coordinate
        Output:
            The nesw rotated so that it can be directly ORed with the edgematrix
    """
    if(trans[1]): #If parity, switch east & west to simulate vertically aligned flip
        nesw=(nesw & 0b1010)+((nesw & 0b0100)>>2)+((nesw & 0b0001)<<2)
    nesw=((0b10001*nesw)>>((4-trans[2])%4))%0b10000 #representation is 0bnesw. change representation to 0bneswnesw so we can do >> bitwise operation. Then shift once per rotation, (4-angle) as doing reverse action, then % back to 0bnesw
    return nesw
    
def picturemake(ImageFile="test.png",SaveFile="savefile.lev",distance="600",clipping="G",pixelwidth="1",alpha_rgb="#FF00FF",centerpixeloffset="0.99984",neswdist="0.45",solidnocolor_rgb="#AA00AA",newversion=True):
    """
        Main program. Takes an image and builds it pixel-by-pixel into an elastomania level file.
        
        Inputs:
            ImageFile (string): Path to the image file to transform, in any common image format. The number of non-transperant pixels should be no more than 5000 or else the level file will have exceeded the maximum number of permitted pictures
            SaveFile (string): Path to the save destination of the generated .lev file.
            distance (string): An integer from 1 to 999 representing the distance of the pictures used to make the image.
            clipping (string): "G", "S", or "U", representing the clipping of the pictures used to make the image
            pixelwidth (string): a float which scales the image. This number is divided by 48 as 48 pixels in elma at 1.00 zoom = 1 elmameter. Should be kept constant but can be used in very specific circumstance. 0.99984 is used because pixelline bugs appear if 1 is used.
            alpha_rgb (string): a hexadecimal string representing the RGB of the alpha channel, or the string "None" if no alpha channel is to be used
            centerpixeloffset (string): A float to align the generated polygons exactly over the pictures. A value of 1.00 is optimal at 1.00 zoom in the game, but because of the bleeding of adjacent pictures pixels, a value of 0.75 appears to work better at increased zooms
            neswdist (string): A float representing the radius of a polygon drawn over a single pixel. Should be between 0<=neswdist<1, ideally 0.45
            solidnocolor_rgb (string): a hexadecimal string representing the RGB of the solid-but-don't-place-a-picture channel, or the string "None" if no channel is to be used
            newversion (bool): True if FancyBoost 2.0, False if FancyPic retro to be used
        Output:
            A .lev file is saved to the indicated path
    """
    logging.info("STARTING ANALYSIS OF NEW IMAGEFILE")
    
    #Convert all inputs from strings to representative numbers
    clipping=clipdic[clipping]
    pixelwidth=float(pixelwidth)/48
    distance=int(distance)
    assert distance>=1 and distance<=999
    if(alpha_rgb=="None"):
        alpha_rgb=False
    else:
        alpha_rgb=Hex2Rgb(alpha_rgb)
    if(solidnocolor_rgb=="None"):
        solidnocolor_rgb=False
    else:
        solidnocolor_rgb=Hex2Rgb(solidnocolor_rgb)
    centerpixeloffset=float(centerpixeloffset)+0.5
    neswdist=float(neswdist) #when drawing polygons, distance from center of pixel to NESW vertex
    
    #Load PaintPixelList, representing data of all usable elma pictures
    if(newversion):
        datafile="fancy_boost.dat"  #2.0
    else:
        datafile="Retro/fancy_retro.dat"  #retro file
    try:
        with open(datafile,"rb") as input:
            PaintPixelList=pickle.load(input)
            logging.debug(PaintPixelList)
        RGBlist=[PaintPixel.rgb for PaintPixel in PaintPixelList] #Make a quick reference list of the RGBs of each item in PaintPixelList
    except:
        ctypes.windll.user32.MessageBoxW(0, "Error: Unable to read file %s. Verify that the file is present in the main directory, and not corrupt!\r\n\r\n%s"%(datafile,sys.exc_info()), "Error!", 0)
        return

    #Open the image and convert to rgb format so that we know that getpixel will return a list of 3. Ask for abort if # of pixels in image is over 100k
    try:
        img=Image.open(ImageFile)
        try:
            assert(img.width*img.height<=100000)
        except AssertionError:
            if(ctypes.windll.user32.MessageBoxW(0, "Error: Width x Height of the image is over 100,000! ("+str(img.width)+"x"+str(img.height)+"="+str(img.width*img.height)+"). Elma can only support images with up to 5000 pixels, not including transperant pixels, or else the level will have an internal error. You most likely must reduce the level size. In addition, this might take many seconds to process. Please abort and reduce the image size.\r\n\r\nAbort? (select Yes)", "Error!", 4)==6):
                return
    except:
        ctypes.windll.user32.MessageBoxW(0, "Error: Unable to open image file! Verify that the path is correct and the file is valid.\r\n\r\n"+str(sys.exc_info()), "Error!", 0)
        return
    try:
        img=img.convert(mode="RGB")
    except:
        ctypes.windll.user32.MessageBoxW(0, "Error: Unable to convert image file into rgb format! Try to copy file into a different image editor and saving.\r\n\r\n"+str(sys.exc_info()), "Error!", 0)
        return

    #Setup simple level
    level = Level()
    level.polygons = [Polygon([Point(-30, -30), Point(30, -30), Point(30, 30), Point(-30, 30)])]
    level.objects = [Obj(Point(0, -2), Obj.START),	Obj(Point(-5, -5), Obj.FLOWER)]
    
    edgematrix = [[[False,0b1111] for y in range(img.height+2)] for x in range(img.width+2)]
    """
        edgematrix is used when generating polygons to keep track of what parts of the image have been processed in the drawing of polygons. It also has a binary map of the image (solid/transperant) for reference.
        Initially, edgematrix is initialized to say that all pixels are transperant (and therefore the whole image is processed as we don't process transperant pixels)
        
        Dimensions: (height+2)x(width+2). A 1-pixel border of empty pixels is added around what will be the main image so that the edge-detection algorithm will interpret the border of the images as empty pixels
        edgematrix[i][j][0] (boolean): True if the corresponding pixel from the image is non-transperant. Otherwise False
        edgematrix[i][j][1] (int) (int): Bitwise binary 0b0000 to 0b1111, representing the cardinal directions nesw. where 1=the programm has processed the corresponding edge of the pixel in question.
                0b1000=north edge, 0b0100 east, 0b0010 south, 0b0001 west (from most significant to least significant is clockwise order from N to W)
    """

    #Cycle through each pixel and put in corresponding picture. Note that in elastomania, earlier pictures in the list appear on top of later pictures of the equivalent distance
    rgb2paintpixel={} #Setup this dictionary to avoid recalculating previously-evaluated colors. {(R,G,B)->index to access correct PaintPixel information from PaintPixelList}. 
    previousIndex=-1    #The color code of the previous pixels
    previousIndexJ=-1   #The j of the last location that has a picture placed
    for i in range(img.height-1,-1,-1):		#work backwards so that the correct images appear on top of each other. Right to left and then upwards
        lastJ=-1
        for j in range(img.width-1,-1,-1):
            target=img.getpixel((j,i))
            if(target==alpha_rgb):
                continue 	#skip drawing picture for transperant pixels
            edgematrix[j+1][i+1]=[True,0]	#If the color is not transperant, change edgematrix to say the pixel is solid, and that the pixel is not processed. +1 because of extra padding around image
            if(target==solidnocolor_rgb):
                continue 	#skip drawing picture for solid pixels that don't have color
            if(not target in rgb2paintpixel):	#If the have not encountered this rgb before (i.e. it's not in our look-up dictionary)
                distlist=[0 for x in range (len(RGBlist))]	#cycle through all possibilities in PaintPixelList's RGBlist and find the color with the minimum distance from the current color
                for k in range(0,len(RGBlist)):
                    distlist[k]=(RGBlist[k][0]-target[0])**2+(RGBlist[k][1]-target[1])**2+(RGBlist[k][2]-target[2])**2
                minIndex=distlist.index(min(distlist))
                rgb2paintpixel[target]=minIndex	#Add new rgb->picture to dictionary, then append corresponding picture to the level
            else:
                minIndex=rgb2paintpixel[target]	#If we already searched this color, then we can just look it up and add it
            if(newversion): #horizontal compression algorithm
                if(minIndex==previousIndex):    #This color is the same color
                    if((previousIndexJ-j)*pixelwidth>ZZ_IMAGE_WIDTH/48): #check if not too long since previous picture placed.
                        level.pictures.append(Picture(Point(lastJ*pixelwidth-PaintPixelList[previousIndex].coord[0]/48,i*pixelwidth-PaintPixelList[previousIndex].coord[1]/48),picture_name=PaintPixelList[previousIndex].name,distance=distance,clipping=clipping))
                        previousIndexJ=lastJ
                else:
                    if(previousIndex!=-1):  #different! write a picture
                        level.pictures.append(Picture(Point(lastJ*pixelwidth-PaintPixelList[previousIndex].coord[0]/48,i*pixelwidth-PaintPixelList[previousIndex].coord[1]/48),picture_name=PaintPixelList[previousIndex].name,distance=distance,clipping=clipping))
                        previousIndexJ=lastJ
                        previousIndex=minIndex
                    else:
                        previousIndex=minIndex  #first element of a row
                        previousIndexJ=j+1  #This is east-most pixel+1 (everything to right is alpha
                lastJ=j #Last j evaluated, skipping alpha holes
            else: #old version - just place it as no compression
                level.pictures.append(Picture(Point(j*pixelwidth-PaintPixelList[minIndex].coord[0]/48,i*pixelwidth-PaintPixelList[minIndex].coord[1]/48),picture_name=PaintPixelList[minIndex].name,distance=distance,clipping=clipping))
        if(newversion): #place west-most pixel
            if(lastJ!=-1):  #Ignore rows of only alpha
                level.pictures.append(Picture(Point(lastJ*pixelwidth-PaintPixelList[previousIndex].coord[0]/48,i*pixelwidth-PaintPixelList[previousIndex].coord[1]/48),picture_name=PaintPixelList[previousIndex].name,distance=distance,clipping=clipping))
                previousIndex=-1
        continue
        
    #Make sure we did not exceed the 5000-picture limit
    picnum=len(level.pictures)
    if(picnum>5000):
        if(ctypes.windll.user32.MessageBoxW(0, "Error: Creating this image required "+str(picnum)+" pixels! Elma can only support images with up to 5000 pixels, so you must reduce the image size. Each dimension of the new image must be LESS THAN "+Truncate(math.sqrt(5000/picnum),5)+" of the original. If you make the level file anyways (NOT RECOMMENDED), you would have to manually delete "+str(picnum-5000)+" pictures from this level in a level editor (e.g. SLE) to avoid an internal error. Please abort and reduce the image size.\r\n\r\nAbort? (select Yes)", "Error!", 4)==6):
            return
    
    
    
    #Draw the polygons around the non-transperant parts of the image.
    for i in range(1,img.height+1):	#skip the outer-most padding layers as we know they represent transperant pixels
        for j in range (1,img.width+1): #Loop through all image pixels, compensating for padding
            if(edgematrix[j][i][0]):	#if pixel is non-transperant
                logging.debug("Checking unprocessed edges for "+str(j)+","+str(i)) #Check each of 4 edges to see if need to draw a polygon or not
                for k in fouredgecoord:	#fouredgecoord has the relative coordinates of each of the 4 sides + the nesw mask
                    logging.debug("Checking edge: "+str(k))
                    if(edgematrix[j][i][1]&k[1]):	#skip if bitwise nesw is 1 meaning that this edge was already processed in a previous loop
                        continue
                    if(edgematrix[j+k[0][0]][i+k[0][1]][0]):	#if edge-pixel is non-transperant, it is not an edge
                        edgematrix[j][i][1]=edgematrix[j][i][1]|k[1]	#mark edge as processed (possibly can omit this line as it might not change rest of processing)
                        continue
                    collisionindex=FindTouchingPixelAround(edgematrix,[j,i],coord2index[(k[0][0],k[0][1])]+1,[0,0,0],1)	#Search counterclockwise from the edge to find the first solid pixel
                    if(collisionindex==-1):	#No solid pixel was found. We have a single non-solid pixel surround by 8 transperant pixels
                        logging.debug("singledot")	#Add a 4-point polygon (order of points on purpose have the wrong orientation because all generated polygons are of wrong orientation and so we will flip all of them at the end)
                        level.polygons.append(Polygon([Point((j-centerpixeloffset)*pixelwidth,(i-centerpixeloffset-neswdist)*pixelwidth),Point((j-centerpixeloffset+neswdist)*pixelwidth,(i-centerpixeloffset)*pixelwidth),Point((j-centerpixeloffset)*pixelwidth,(i-centerpixeloffset+neswdist)*pixelwidth),Point((j-centerpixeloffset-neswdist)*pixelwidth,(i-centerpixeloffset)*pixelwidth)]))
                        edgematrix[j][i][1]=0b1111	#(possibly can omit this as it might not change the rest of processing)
                        continue
                    transform=first_vertex_transform_d[collisionindex] #we found the solid pixel and will now lookup the transformation needed to start the polygon. We have found the pixel around. (see documentation for first_vertex_transform)
                    new_poly=Polygon([])	#This will be our new polygon
                    x=j		#need to save i and j (?probably not necessary anymore now that we use a for loop instead of while loop. possibly omit), but need an x/y for tracking around the edge
                    y=i
                    logging.debug("Begin polygon-drawing")
                    while True:
                        logging.debug("-----New cycle")		#loop for each pixel of the polygon, with current position in [x,y] and the transformation used to analyze the situation in variable transform=[location,parity,angle]
                        logging.debug("Coord:"+str(x)+","+str(y))	#with transform, we can move the pixelaround into the N or NE position to analyze
                        logging.debug("Transform:"+str(transform))
                        if(transform[0]):	#pixel around is diagonal, and so placed in NE position
                            nextsolid=FindTouchingPixelAround(edgematrix,[x,y],1,transform,1)	#search counterclockwise starting from index 1 to find the 2nd pixel around to lookup what to do from our dictionary
                            instructions=next_vertex_transform_dia_d[nextsolid] #(see documentation for next_vertex_transform)
                        else:				#adjacent pixel around
                            nextsolid=FindTouchingPixelAround(edgematrix,[x,y],5,transform,-1) 	#search clockwise starting from index 5 (see documentation for next_vertex_transform)
                            instructions=next_vertex_transform_adj_d[nextsolid]
                        logging.debug("Next solid index:"+str(nextsolid))
                        logging.debug("Obtained instructions:"+str(instructions))
                        
                        #now we've put the first pixel_around at the top, and found the 2nd pixel_around and obtained the information on what to do from the dictionary. Make sure that this information is not about the very first pixel we already analyzed for this polygon
                        #verify that if we OR the current pixel's nesw, it causes a new result. If the OR doesn't change the result, it means we've already processed this pixel (i.e. we are back to the original pixel)
                        nesw_absolute=BinaryRotate(instructions[0],transform)	#rotate nesw to fit the real situation
                        logging.debug("binary compare: ("+bin(instructions[0])+"->"+bin(nesw_absolute)+" | "+bin(edgematrix[x][y][1])+"->"+bin(edgematrix[x][y][1]|nesw_absolute))
                        if(edgematrix[x][y][1]|nesw_absolute==edgematrix[x][y][1]): #OR makes no difference = already did this pixel
                            logging.debug("Polyend:" + str(new_poly.points))
                            if(len(new_poly.points)>=3):
                                level.polygons.append(new_poly)	#insert the polygon into the level and move on to check the next edge/pixel for a new polygon
                            else:
                                logging.error("??Invalid polygon generated. Trashing current polygon. Please report this error. FancyPic will attempt to complete the process anyways")
                            break
                        edgematrix[x][y][1]=edgematrix[x][y][1]|nesw_absolute	#apply nesw OR
                        
                        #Add the vertices required to draw the current pixel
                        for l in instructions[1]:
                            rotatecoord=CoordRotate(l,transform)
                            logging.debug("Draw:"+str(l)+"/"+str(transform)+"->"+str(x-centerpixeloffset+neswdist*rotatecoord[0])+","+str(y-centerpixeloffset+neswdist*rotatecoord[1]))
                            new_poly.points.append(Point((x-centerpixeloffset+neswdist*rotatecoord[0])*pixelwidth,(y-centerpixeloffset+neswdist*rotatecoord[1])*pixelwidth))

                        #now choose the next pixel. We know where it is because we have the index of the second pixel_around which is where we are heading next. we just need to rotate back to the absolute edgematrix reference frame
                        temp=CoordRotate(index2coord[nextsolid],transform)
                        x=x+temp[0]	#move to the new coordinate
                        y=y+temp[1]

                        #look-up the transformation for the next center pixel. depending on where the known transperant pixels are, we have to flip differently (see documentation first_vertex_transform)
                        if((transform[0]+transform[1])%2): #if transform[0] XOR transform[1] (if the pixel center we are leaving involved diagonal or parity), then we should be using the clockwise index lookup
                            #the previous central pixel is now the starting pixel. we know the relative position through (it is -temp). we can look up the absolute index and figure out the starting transformation
                            #can possibly delete this if statement as ALL polygons are generated using counterclockwise lookup
                            logging.error("Clockwise next pixel: index:"+str(coord2index[(-temp[0],-temp[1])]))
                            logging.error("Please report this error")
                            transform=first_vertex_transform_rev_d[coord2index[(-temp[0],-temp[1])]]
                        else:
                            #logically, since we started going counterclockwise, I think we will ALWAYS continue counterclockwise, but I'm leaving the code for clockwise in case I'm wrong. Will later probably omit the clockwise side and just always assume ccw.
                            #ccw
                            logging.debug("Counterclockwise next pixel: index:"+str(coord2index[(-temp[0],-temp[1])]))
                            transform=first_vertex_transform_d[coord2index[(-temp[0],-temp[1])]]
    logging.debug(level.polygons)
    
    #all the generated polygons are made in the wrong orientation (except the original first level polygon)
    #flip the orientation. Since we know all the polygons are drawn in the wrong orientation, we don't need to manually check. The code to manually check was written in Version 4, and is seen in 
    #Smibu's SLE: https://gitlab.com/Smibu/elmanager/blob/master/Level.cs, under "Save". I deleted the code because in the end didn't need to check
    for i in range(1,len(level.polygons)):
        level.polygons[i].points.reverse()
        clipping=0
    
    try:
        with open(SaveFile, 'wb') as f:
            f.write(pack_level(level))
        ctypes.windll.user32.MessageBoxW(0, "Success! %s pictures used to generate image"%(picnum), "Yay!", 0)
    except:
        ctypes.windll.user32.MessageBoxW(0, "Error: Unable to save level file! Verify that the file is not being used and that the path is valid.\r\n"+str(sys.exc_info()), "Error!", 0)
    return
    
clipdic={"G":Picture.CLIPPING_G,"U":Picture.CLIPPING_U,"S":Picture.CLIPPING_S}	#Convert letters from GUI into numbers
cosd={0:int(math.cos(math.radians(0))),1:int(math.cos(math.radians(90))),2:int(math.cos(math.radians(180))),3:int(math.cos(math.radians(270)))} #precalculate cos/sin of 90 degree angles and round to exact values for cleaner code/faster execution
sind={0:int(math.sin(math.radians(0))),1:int(math.sin(math.radians(90))),2:int(math.sin(math.radians(180))),3:int(math.sin(math.radians(270)))}
fouredgecoord=[[[0,-1],0b1000],[[1,0],0b0100],[[0,1],0b0010],[[-1,0],0b0001]] #a list of the coordinates of the 4 edges with the corresponding nesw bitmask

index2coord={0:(0,-1),1:(-1,-1),2:(-1,0),3:(-1,1),4:(0,1),5:(1,1),6:(1,0),7:(1,-1)} #counterclockwise association of coordinates by index number. See documentation "index2coord"
coord2index={index2coord[k] : k for k in index2coord}	#reversed dictionary

first_vertex_transform_d={
    0:[0,0,0],		#The index represents the solid pixel that was found in a counterclockwise search method.
    1:[1,1,0],		#The dictionary returns the transformation to put that solid pixel in position with the correct side of the pixel given the search method
    2:[0,0,1],		#(see documentation first_vertex_transform)
    3:[1,1,1],
    4:[0,0,2],
    5:[1,1,2],
    6:[0,0,3],
    7:[1,1,3]}
first_vertex_transform_rev_d={
    0:[0,1,0],		#Same as first_vertex_transform_d but if clockwise search method was used
    1:[1,0,1],
    2:[0,1,1],
    3:[1,0,2],
    4:[0,1,2],
    5:[1,0,3],
    6:[0,1,3],
    7:[1,0,0]}
    
    
next_vertex_transform_adj_d={
    #index represents solid pixel found when the first pixelaround is adjacent and placed in index 0, and the second pixel is found clockwise and found at index n
    # see documentation next_vertex_transform
    #[0]=unrotated OR mask to apply to edgematrix (rotate first)
    #[1]=list of relative locations of vertices to add (non-rotated)
    #transformation matrix of the subsequent pixel in the next iteration
    5:[0b1100,[[1,0]],[1,1,0]],
    4:[0b1110,[],[0,0,0]],
    3:[0b1110,[[1,0]],[1,1,3]],
    2:[0b1111,[[1,0],[0,1]],[0,0,3]],
    1:[0b1111,[[1,0],[0,1]],[1,1,2]],
    0:[0b1111,[[1,0],[0,1],[-1,0]],[0,0,2]]}
next_vertex_transform_dia_d={
    #Ditto when initial pixel is diagonal. see documentation
    1:[0b1000,[[0,-1]],[1,0,3]],
    2:[0b1001,[[0,-1]],[0,1,3]],
    3:[0b1001,[],[1,0,0]],
    4:[0b1011,[[-1,0]],[0,1,0]],
    5:[0b1011,[[-1,0]],[1,0,1]],
    6:[0b1111,[[-1,0],[0,1]],[0,1,1]],
    7:[0b1111,[[-1,0],[0,1],[1,0]],[1,0,2]]}


    
    #Estimated width of maskhor=28.562-21.5
    #pixel width of maskhor=339
    #estimated x_distance/pixel=0.02
    #manual testing gave suggestion of pixelwidth=0.02083 #somewhere between 0.02075-0.02088 is optimal
    #According to Domi's Big Book of Elma Facts, 48 pixels = 1 elmameter, i.e. 1/48 = 0.20833333
    
    

    # RGB Palate from default.lgr's fancy_lgr_data.dat:
    # 0,0,0
    # 0,8,8
    # 120,0,0
    # 128,0,0
    # 0,96,8
    # 128,128,128
    # 0,48,120
    # 0,80,0
    # 48,156,48
    # 196,0,0
    # 0,32,0
    # 24,8,0
    # 136,0,0
    # 72,0,0
    # 156,8,0
    # 88,24,16
    # 104,48,16
    # 40,16,8
    # 0,16,0