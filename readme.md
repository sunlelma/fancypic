# FancyPic

FancyPic is a program written by sunl to create custom images in elastomania level files.


[Click here for discussion](http://mopolauta.moposite.com/viewtopic.php?f=3&t=9611)

## Set-up from source documents

Programmed on Python 3.5.0 (64-bit).

Requires the following packages to be installed (newer packages probably work as well)

 * pip install elma==0.5
 * pip install Pillow==4.0.0
 * pip install PyInstaller==3.2

You can then run the program the following ways:

 1. Run fancypic.py
 2. Run test.py to auto-test "test.png" using default settings
 3. Run generate_exe.bat to generate the distributable .exe version (created in the "dist" folder) (exe file cannot be compiled on 3.6 as PyInstaller does not exist for 3.6, but otherwise should work fine)