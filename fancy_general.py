class PaintPixel(object):
	"""
		Represents data from 1 lgr picture; about the leftmost pixel on the topmost row, excluding invisible pixels (referred to as pixel TL). These objects are used to contain data about 
		
		Attributes:
			mypalette (int): A number from 0-255 corresponding to the palette id of pixel TL. The overall palette is the same within one lgr.
			coord ((i,j))
				i (int): The x coordinate of the location of pixel TL within the lgr picture from the left
				j (int): The y coordinate of the location of pixel TL within the lgr picture from the top
			rgb ([R,G,B]):
				R,G,B (int): the rgb values of the color of pixel TL
			name (string): The name of the picture resource, without .pcx/.bmp
			width (int): The width of the lgr picture
			height (int): The height of the lgr picture
	"""
	def __init__(self,mypalette,coord,rgb,name,width,height):
		self.mypalette = mypalette
		self.coord=coord
		self.rgb = rgb
		self.name = name
		self.width = width
		self.height = height
	def __repr__(self):
		return (
			('PaintPixel(mypalette: %s, coord: %s, rgb: %s, name: %s, width: %s, height: %s)') % (self.mypalette, self.coord, self.rgb, self.name, self.width, self.height))
	def __eq__(self, other):
		return (self.name == other.name)
		
def Hex2Rgb(value):
	"""
		Convert a string representing rgb values into a tuple representing this rgb value.
		
		Inputs:
			value (string): A string formatted to represent an RGB value in hexadecimal, with or without leading # (e.g. "#FFFFFF" or "FFFFFF")
		Output:
			(R,G,B) (int,int,int): A tuple representing the RGB value
	"""
	value = value.lstrip('#')
	assert(len(value)==6)
	return tuple(int(value[i:i + 2], 16) for i in range(0, 6, 2))
	
def Truncate(f, n):
    '''Truncates/pads a float f to n decimal places without rounding'''
	# http://stackoverflow.com/questions/783897/truncating-floats-in-python
    s = '{}'.format(f)
    if 'e' in s or 'E' in s:
        return '{0:.{1}f}'.format(f, n)
    i, p, d = s.partition('.')
    return '.'.join([i, (d+'0'*n)[:n]])