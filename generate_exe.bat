@echo This will delete savefile.lev, fancypic.spec, dist/ and build/ then generate the distributable program in dist/
@pause
rmdir dist /s /q
rmdir build /s /q
del savefile.lev
del fancypic.spec

pyinstaller --onefile fancypic.py

copy /y fancy_boost.dat dist\fancy_boost.dat
copy /y LGR_Booster_Patch_64.exe dist\LGR_Booster_Patch_64.exe
copy /y readme_dist.txt dist\readme.txt
xcopy /y /e GimpPalettes dist\GimpPalettes\
xcopy /y /e Samples dist\Samples\
xcopy /y /e Retro dist\Retro\

@pause